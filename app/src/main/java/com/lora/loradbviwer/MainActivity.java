package com.lora.loradbviwer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.lora.loradbviwer.adapter.ListSensorValuesAdapter;
import com.lora.loradbviwer.database.DatabaseHelper;
import com.lora.loradbviwer.database.SensorDbHelper;
import com.lora.loradbviwer.model.SensorValue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class MainActivity extends Activity {
    private ListView lvSensorValues;
    private ListSensorValuesAdapter adapter;
    private List<SensorValue> sensorValues;
    private DatabaseHelper mDBHelper;
    private SensorDbHelper sensorDbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String startDate = intent.getStringExtra("startDate");
        String endDate = intent.getStringExtra("endDate");
        int sensorId = intent.getIntExtra("sensorId", 0);
        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);

        setContentView(R.layout.activity_main);
        lvSensorValues = (ListView)findViewById(R.id.listview_sensor_values);
        mDBHelper = new DatabaseHelper(this);

        //Check exists database
        File database = getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);
        if(false == database.exists()) {
            mDBHelper.getReadableDatabase();
            //Copy db
            if(copyDatabase(this)) {
                Toast.makeText(this, "Copy database succes", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Copy data error", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //Get product list in db when db exists
        sensorDbHelper = new SensorDbHelper(this);
        sensorValues = sensorDbHelper.getSensorValues(startDate, endDate, sensorId);
        //Init adapter
        adapter = new ListSensorValuesAdapter(this, sensorValues);
        //Set adapter for listview
        lvSensorValues.setAdapter(adapter);

    }

    private boolean copyDatabase(Context context) {
        try {

            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[]buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("MainActivity","DB copied");
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
