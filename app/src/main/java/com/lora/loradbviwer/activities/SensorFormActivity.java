package com.lora.loradbviwer.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.lora.loradbviwer.MainActivity;
import com.lora.loradbviwer.R;
import com.lora.loradbviwer.helpers.DateFormatter;

import java.util.Calendar;
import java.util.Date;

public class SensorFormActivity extends AppCompatActivity implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener {

    private final AppCompatActivity activity = SensorFormActivity.this;

    private TextView dateTextView;
    private TextView timeTextView;
    private AppCompatButton searchBtn;
    private Spinner spinner;

    private String startDate;
    private String startTime;
    private String startDateTime;
    private String endDateTime;
    private String endDate;
    private String endTime;
    private int selectedSensor = 0;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.form_search_btn:
                displaySensorsList();
                break;
        }
    }

    private void displaySensorsList() {
        Intent sensorValueList = new Intent(activity, MainActivity.class);
        sensorValueList.putExtra("startDate", this.startDateTime);
        sensorValueList.putExtra("endDate", this.endDateTime);
        sensorValueList.putExtra("sensorId", this.selectedSensor);
        startActivity(sensorValueList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sensor_form_activity);

        this.startDateTime = DateFormatter.format(new Date());
        this.endDateTime = DateFormatter.format(new Date());

        searchBtn = (AppCompatButton) findViewById(R.id.form_search_btn);
        spinner = (Spinner) findViewById(R.id.selected_sensor);

        // Find our View instances
        dateTextView = (TextView)findViewById(R.id.date_textview);
        timeTextView = (TextView)findViewById(R.id.time_textview);
        Button dateButton = (Button)findViewById(R.id.date_button);
        Button timeButton = (Button)findViewById(R.id.time_button);

        // Show a datepicker when the dateButton is clicked
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        SensorFormActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAutoHighlight(true);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        timeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        SensorFormActivity.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Log.d("TimePicker", "Dialog was cancelled");
                    }
                });
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedSensor = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        initListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth,int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String date = "Selected date : From- "+dayOfMonth+"/"+(++monthOfYear)+"/"+year+" To "+dayOfMonthEnd+"/"+(++monthOfYearEnd)+"/"+yearEnd;
        dateTextView.setText(date);

        this.startDate = monthOfYear + "/" + dayOfMonth + "/" + year;
        this.endDate = monthOfYearEnd + "/" + dayOfMonthEnd + "/" + yearEnd;

        this.startDateTime = this.startDate + this.startTime;
        this.endDateTime = this.endDate + this.endTime;
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0"+hourOfDayEnd : ""+hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0"+minuteEnd : ""+minuteEnd;
        String time = "Selected time : From - "+hourString+"h"+minuteString+" To - "+hourStringEnd+"h"+minuteStringEnd;

        timeTextView.setText(time);

        this.startTime = " " + hourString + ":" + minuteString + ":00";
        this.endTime = " " + hourStringEnd + ":" + minuteStringEnd + ":00";

        this.startDateTime = this.startDate + this.startTime;
        this.endDateTime = this.endDate + this.endTime;
    }

    private void initListeners() {
        searchBtn.setOnClickListener(this);
    }
}
