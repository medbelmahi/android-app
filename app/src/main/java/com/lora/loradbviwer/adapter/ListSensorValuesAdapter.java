package com.lora.loradbviwer.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lora.loradbviwer.R;
import com.lora.loradbviwer.helpers.DateFormatter;
import com.lora.loradbviwer.model.SensorValue;

import java.util.List;

public class ListSensorValuesAdapter extends BaseAdapter {
    private Context mContext;
    private List<SensorValue> sensorValueList;

    public ListSensorValuesAdapter(Context mContext, List<SensorValue> sensorValueList) {
        this.mContext = mContext;
        this.sensorValueList = sensorValueList;
    }

    @Override
    public int getCount() {
        return sensorValueList.size();
    }

    @Override
    public Object getItem(int position) {
        return sensorValueList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return sensorValueList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = View.inflate(mContext, R.layout.item_listview, null);

        TextView date = (TextView) v.findViewById(R.id.date_time);
        TextView sensorId = (TextView) v.findViewById(R.id.sensor_id);
        TextView smsValue = (TextView) v.findViewById(R.id.sms_value);
        TextView temperatureValue = (TextView) v.findViewById(R.id.temperature_value);
        TextView waterLevelMessage = (TextView) v.findViewById(R.id.water_level_message);

        SensorValue sensorValue = sensorValueList.get(position);
        date.setText(DateFormatter.format(sensorValue.getDate()));
        sensorId.setText(sensorValue.printSensorId());
        smsValue.setText(sensorValue.printSmsValue());
        temperatureValue.setText(sensorValue.printTemperatureValue());
        waterLevelMessage.setText(sensorValue.getWaterLevelMessage());

        return v;
    }
}
