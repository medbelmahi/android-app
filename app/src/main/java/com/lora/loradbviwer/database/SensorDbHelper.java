package com.lora.loradbviwer.database;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import com.lora.loradbviwer.helpers.DateFormatter;
import com.lora.loradbviwer.model.SensorValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SensorDbHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "lora.sqlite";
    public static final String DBLOCATION = "/data/data/com.lora.loradbviwer/databases/";
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public SensorDbHelper(Context context) {
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DBNAME).getPath();
        if(mDatabase != null && mDatabase.isOpen()) {
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    /*public List<SensorValue> getSensorValues() {
        SensorValue sensorValue_1 = new SensorValue(1, new Date(), 1, "here 1", 800, 10);
        SensorValue sensorValue_2 = new SensorValue(1, new Date(), 2, "here 1", 800, 10);
        return Arrays.asList(sensorValue_1, sensorValue_2);
    }*/

    public List<SensorValue> getSensorValues(String startDate, String endDate, int sensorId) {
        SensorValue sensorValue = null;
        List<SensorValue> sensorValues = new ArrayList<>();
        openDatabase();
        Cursor cursor;
        if (sensorId == 0) {
            cursor = mDatabase.rawQuery("SELECT * FROM SENSOR", null);
        }else {
            String[] selectionArgs = {Integer.valueOf(sensorId).toString()};
            cursor = mDatabase.rawQuery("SELECT * FROM SENSOR WHERE sensorId = ?", selectionArgs);
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            sensorValue = new SensorValue(cursor.getInt(0), DateFormatter.parser(cursor.getString(1)),
                    cursor.getInt(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5));
            sensorValues.add(sensorValue);
            cursor.moveToNext();
        }

        cursor.close();
        closeDatabase();


        final Date theStartDate = DateFormatter.parser(startDate);
        final Date theEndDate = DateFormatter.parser(endDate);

        List<SensorValue> newList = new ArrayList<>();
        for (SensorValue value : sensorValues) {
            if (value.getDate().after(theStartDate) && value.getDate().before(theEndDate)) {
                newList.add(value);
            }
        }

        return newList;
    }


    public void closeDatabase() {
        if(mDatabase!=null) {
            mDatabase.close();
        }
    }
}
