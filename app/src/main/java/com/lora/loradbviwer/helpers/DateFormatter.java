package com.lora.loradbviwer.helpers;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {
    public static final String DATE_FORMAT = "dd-MM-yyyy 'at' HH:mm";

    public static String format(Date date) {
        return new SimpleDateFormat(DATE_FORMAT,
                Locale.ENGLISH).format(date);
    }

    public static Date parser(String input) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        try {
            return  sdf.parse(input);
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
        }

        return new Date();
    }
}
