package com.lora.loradbviwer.model;

import java.util.Date;

public class SensorValue {

    private int id;
    private Date date;
    private int sensorId;
    private String waterLevelMessage;
    private int smsValue;
    private int temperatureValue;

    public SensorValue(int id, Date date, int sensorId, String waterLevelMessage, int smsValue, int temperatureValue) {
        this.id = id;
        this.date = date;
        this.sensorId = sensorId;
        this.waterLevelMessage = waterLevelMessage;
        this.smsValue = smsValue;
        this.temperatureValue = temperatureValue;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public String getWaterLevelMessage() {
        return waterLevelMessage;
    }

    public void setWaterLevelMessage(String waterLevelMessage) {
        this.waterLevelMessage = waterLevelMessage;
    }

    public int getSmsValue() {
        return smsValue;
    }

    public void setSmsValue(int smsValue) {
        this.smsValue = smsValue;
    }

    public int getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(int temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    public String printSensorId(){
        return "Sensor " + sensorId;
    }

    public String printSmsValue() {
        return "Sms Value : " + smsValue;
    }

    public String printTemperatureValue() {
        return "Temperature Value : " + temperatureValue;
    }
}
